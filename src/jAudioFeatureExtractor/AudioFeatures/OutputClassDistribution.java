/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jAudioFeatureExtractor.AudioFeatures;

/**
 *
 * @author Olajide Martins
 */
import jAudio.org.oc.ocvolume.dsp.featureExtraction;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.io.File;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.TargetDataLine;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.Utils;
import weka.core.converters.ConverterUtils.DataSource;

/**
 * This example class trains a J48 classifier on a dataset and outputs for 
 * a second dataset the actual and predicted class label, as well as the 
 * class distribution.
 * 
 * @author  FracPete (fracpete at waikato dot ac dot nz)
 * @version $Revision: 5628 $
 */
public class OutputClassDistribution {
 private static final double[] FREQUENCIES = { 174.61, 164.81, 155.56, 146.83, 138.59, 130.81, 123.47, 116.54, 110.00, 103.83, 98.00, 92.50, 87.31, 82.41, 77.78};
    private static final String[] NAME        = { "F",    "E",    "D#",   "D",    "C#",   "C",    "B",    "A#",   "A",    "G#",   "G",   "F#",  "F",   "E",   "D#"};
    int j=0;
    public static class Graph extends JPanel {
        private java.util.List<Double> points = new ArrayList<Double>();
        private java.util.List<Integer> markers = new ArrayList<Integer>();
        
        public Graph() {
            setPreferredSize(new Dimension(320,100));
        }
        
        public synchronized void clear() {
            points.clear();
            markers.clear();
        }
        
        public synchronized void add(double value) {
            points.add(value);
        }
        
        public synchronized void mark(int pos) {
            markers.add(pos);
        }
        
        public synchronized void paint(Graphics g) {
            g.setColor(Color.BLACK);
            
            double min = Double.MAX_VALUE, max = -Double.MAX_VALUE;
            for ( double p: points ) {
                min = Math.min(p, min);
                max = Math.max(p, max);
            }
            
            double width  = getWidth();
            double height = getHeight();
            
            g.clearRect(0,0,(int)width,(int)height);
            g.drawRect(0,0,(int)width,(int)height);
            
            
            double prevY = 0, prevX = 0;
            boolean first = true;
            
            int ix = 0;
            for ( double p: points ) {
                double y = height - (height*(p-min)/(max-min));
                double x = (width*ix)/points.size();
                
                if ( !first ) {
                    g.drawLine((int)prevX,(int)prevY,(int)x,(int)y);
                }
                
                first = false;
                prevY = y;
                prevX = x;
                ix++;
            }
            
            double zero = height - (height*(0-min)/(max-min));
            g.drawLine(0,(int)zero,(int)width,(int)zero);
            
            g.setColor(Color.RED);
            for ( int pos: markers ) {
                double x = (width*pos)/points.size();
                g.drawLine((int)x, 0, (int)x, (int)height);
            }
            
        }
    }
    
    private static double normaliseFreq(double hz) {
        // get hz into a standard range to make things easier to deal with
        while ( hz < 82.41 ) {
            hz = 2*hz;
        }
        while ( hz > 164.81 ) {
            hz = 0.5*hz;
        }
        return hz;
    }
    
    private static int closestNote(double hz) {
        double minDist = Double.MAX_VALUE;
        int minFreq = -1;
        for ( int i = 0; i < FREQUENCIES.length; i++ ) {
            double dist = Math.abs(FREQUENCIES[i]-hz);
            if ( dist < minDist ) {
                minDist=dist;
                minFreq=i;
            }
        }
        
        return minFreq;
    }
    
  /**
   * Expects two parameters: training file and test file.
   * 
   * @param args	the commandline arguments
   * @throws Exception	if something goes wrong
   */
  public static void main(String[] args) throws Exception {
      JOptionPane.showInputDialog("ENTER WEB ADDRESS");
        Instances train = DataSource.read("C:\\Users\\Olajide Martins\\Documents\\NetBeansProjects\\MARCHINE\\age.arff");
    train.setClassIndex(train.numAttributes() - 1);
      
      int k = 0;  //counts the number of input samples required
      double predd = 0;
      
       JOptionPane.showMessageDialog(null, "PLEASE SHORTLY SING THE WORDS 'WE ARE THE WORLD'\n YOUR VOICE DOESENT MATTER TO US :)\n BUT PLEASE SING IT");
        Font font = new Font("sansserif", Font.PLAIN, 24);
        Font bigFont = new Font("sansserif", Font.PLAIN, 48);
        
        JFrame frame = new JFrame("5KTuner");
       // frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 500);
        Graph graph1 = new Graph();
        frame.add(graph1, BorderLayout.CENTER);
        
        JLabel matchLabel = new JLabel("--");
        matchLabel.setFont(bigFont);
        JLabel prevLabel = new JLabel("--");
        prevLabel.setFont(font);
        JLabel nextLabel = new JLabel("--");
        nextLabel.setFont(font);
        
        int FREQ_RANGE = 128;
        
        JSlider freqSlider = new JSlider(JSlider.HORIZONTAL, -FREQ_RANGE, FREQ_RANGE, 0);
        
        java.util.Hashtable labels = new java.util.Hashtable();
        labels.put(0, matchLabel);
        labels.put(-FREQ_RANGE, nextLabel);
        labels.put(FREQ_RANGE, prevLabel);
        freqSlider.setLabelTable(labels);
        freqSlider.setPaintLabels(true);
        freqSlider.setPaintTicks(true);
        freqSlider.setMajorTickSpacing(FREQ_RANGE/2);
        freqSlider.setMinorTickSpacing(FREQ_RANGE/8);
        
        frame.add(freqSlider, BorderLayout.NORTH);
        
        JLabel freqLabel = new JLabel("--");
        freqLabel.setFont(new Font("sansserif", Font.PLAIN, 14));
        frame.add(freqLabel, BorderLayout.SOUTH);
        
        frame.pack();
        frame.setVisible(true);
        
        float sampleRate = 44100;
        int sampleSizeInBits = 16;
        int channels = 1;
        boolean signed = true;
        boolean bigEndian = false;
        AudioFormat format = new AudioFormat(sampleRate, sampleSizeInBits, channels, signed, bigEndian);
        DataLine.Info dataLineInfo = new DataLine.Info(TargetDataLine.class, format);
        TargetDataLine targetDataLine = (TargetDataLine)AudioSystem.getLine(dataLineInfo);
        
        // read about a second at a time
        targetDataLine.open(format, (int)sampleRate);
        targetDataLine.start();
        
        byte[] buffer = new byte[2*1200];
         short[] a = new short[buffer.length/2];
        
        int n = -1;
        while ( (n = targetDataLine.read(buffer, 0, buffer.length)) > 0 ) {
            featureExtraction fe = new featureExtraction();
            for ( int i = 0; i < n; i+=2 ) {
                // convert two bytes into single value
                short value = (short)((buffer[i]&0xFF) | ((buffer[i+1]&0xFF) << 8));
                a[i >> 1] = value;
                 System.out.println(Arrays.toString(fe.process(a, sampleRate)));
                
            }
            
           
            
            double prevDiff = 0;
            double prevDx = 0;
            double maxDiff = 0;
            
            int sampleLen = 0;
            
            
            graph1.clear();
            int len = a.length/2;
            for ( int i = 0; i < len; i++ ) {
                double diff = 0;
                for ( int j = 0; j < len; j++ ) {
                    diff += Math.abs(a[j]-a[i+j]);
                }
                
                graph1.add(diff);
                
                double dx = prevDiff-diff;
                
                // change of sign in dx
                if ( dx < 0 && prevDx > 0 ) {
                    // only look for troughs that drop to less than 10% of peak
                    if ( diff < (0.1*maxDiff) ) {
                        graph1.mark(i-1);
                        if ( sampleLen == 0 ) {
                           sampleLen=i-1;
                           
                        }
                    }
                }
                
                prevDx = dx;
                prevDiff=diff;
                maxDiff=Math.max(diff,maxDiff);
            }
            graph1.repaint();
             
            if ( sampleLen > 0 ) {
                double frequency = (format.getSampleRate()/sampleLen);
                freqLabel.setText(String.format("%.2fhz",frequency));
                
                System.out.println(frequency);
                String values = String.valueOf("\n"+"50,"+frequency+",96,"+"adult"); 
          try{
             RandomAccessFile lfile  = new RandomAccessFile("text.arff", "rw");
             lfile.seek(lfile.length());
             lfile.writeBytes(values);
            
             System.out.println("details stored");
                   
    // load data
      Instances test = DataSource.read("C:\\Users\\Olajide Martins\\Documents\\NetBeansProjects\\WEKA\\text.arff");
    test.setClassIndex(test.numAttributes() - 1);
    if (!train.equalHeaders(test))
      throw new IllegalArgumentException(
	  "Train and test set are not compatible!");
    
    // train classifier
    J48 cls = new J48();
    cls.buildClassifier(train);
   
    // output predictions
    System.out.println("# - actual - predicted - error - distribution");
  for (int i = 0; i < test.numInstances(); i++) {
      double pred = cls.classifyInstance(test.instance(i));
      double[] dist = cls.distributionForInstance(test.instance(i));
      System.out.print((i+1));
      System.out.print(" - ");
      System.out.print(test.instance(i).toString(test.classIndex()));
      System.out.print(" - ");
      System.out.print(test.classAttribute().value((int) pred));
      System.out.print(" - ");
      k =i;
      System.out.println(k);
      if (pred != test.instance(i).classValue())
	System.out.print("yes");
      else
	System.out.print("no");
      System.out.print(" - ");
      System.out.print(Utils.arrayToString(dist));
      System.out.println();
         if(pred==0){
         System.out.println("welcome to pooon adult.. God save your life"+pred);
         
//         if  (Desktop.isDesktopSupported()){
//             Desktop open = Desktop.getDesktop();
//            // open.browse(null);
//             open.open(new File("C:\\Users\\Olajide Martins\\Downloads\\D'banj - Wikipedia, the free encyclopedia"));
        // }
//         
     }
     else{
         System.out.println("Don't even think about it kiddo"+pred);
//         if  (Desktop.isDesktopSupported()){
//             Desktop open = Desktop.getDesktop();
//             open.open(new File("C:\\Users\\Olajide Martins\\Downloads\\D'banj - Wikipedia, the free encyclopedia"));
//         }
     }
     String valve;
     if(pred>0){
          valve = "child";
          predd =  pred;
     }
     else{
         valve = "adult";
         predd = pred;
     }
    String valuess = String.valueOf("\n"+"50,"+frequency+",96,"+valve+"\n"); 
          try{
             RandomAccessFile ifile  = new RandomAccessFile("age.arff", "rw");
             ifile.seek(lfile.length());
             ifile.writeBytes(values);
            
             System.out.println("details stored");
         // Runtime.getRuntime().exec("C:\\Users\\Olajide Martins\\Desktop\\CHRISTMAS.txt");
          }
          catch(Exception e){}
//    GT tesst = new GT();
//    tesst.man();
    
    }
             
             if((k==2)&&(predd <1)){
                 frame.setVisible(false);
                 if(Desktop.isDesktopSupported()){
            Desktop dp = Desktop.getDesktop();
            dp.open(new File("C:\\Users\\Olajide Martins\\Downloads\\Solved problems.htm"));
            System.exit(0);
        }
                 
                 else
                 {
                  JOptionPane.showMessageDialog(null, "ODE MALLE LE YI SHA");
                 }
             }
          //   Runtime.getRuntime().exec("C:\\Users\\Olajide Martins\\Desktop\\CHRISTMAS.txt");
             
         }catch(Exception e){
             System.out.println("could not write");
         }
                frequency = normaliseFreq(frequency);
                int note = closestNote(frequency);
                matchLabel.setText(NAME[note]);
                prevLabel.setText(NAME[note-1]);
                nextLabel.setText(NAME[note+1]);
                
                int value = 0;
                double matchFreq = FREQUENCIES[note];
                if ( frequency < matchFreq ) {
                    double prevFreq = FREQUENCIES[note+1];
                    value = (int)(-FREQ_RANGE*(frequency-matchFreq)/(prevFreq-matchFreq));
                    
                }
                else {
                    double nextFreq = FREQUENCIES[note-1];
                    value = (int)(FREQ_RANGE*(frequency-matchFreq)/(nextFreq-matchFreq));
                }
                freqSlider.setValue(value);
            }
            else {
              
                matchLabel.setText("--");
                prevLabel.setText("--");
                nextLabel.setText("--");
                freqSlider.setValue(0);
                freqLabel.setText("--");
            }
            prevLabel.setSize(prevLabel.getPreferredSize());
            nextLabel.setSize(nextLabel.getPreferredSize());
            matchLabel.setSize(matchLabel.getPreferredSize());
            
            freqSlider.repaint();
            freqLabel.repaint();
            
       try { Thread.sleep(250); }catch( Exception e ){}
     }
    }
      

  }

